import java.util.ArrayList;
import java.util.Comparator;

class BuildTree {

    TreeNode buildTree(ArrayList<Record> records) throws InvalidRecordsException {

        if (records.isEmpty()) {
            return null;
        }

        records.sort(Comparator.comparing(Record::getRecordId));

        Record parent = records.get(0);
        int n = records.size();
        if (parent.getRecordId() != 0 || parent.getParentId() != 0) {
            throw new InvalidRecordsException("Invalid Records");
        }
        if (records.get(n - 1).getRecordId() != n - 1) {
            throw new InvalidRecordsException("Invalid Records");
        }
        for (int i = 1; i < n; i++) {
            if (records.get(i).getRecordId() <= records.get(i).getParentId()) {
                throw new InvalidRecordsException("Invalid Records");
            }
        }

        ArrayList<TreeNode> treeNodes = new ArrayList<>();
        treeNodes.add(new TreeNode(0));

        for (int i = 1; i < n; i++) {
            Record record = records.get(i);
            TreeNode node = new TreeNode(record.getRecordId());
            treeNodes.add(node);
            TreeNode parentNode = treeNodes.get(record.getParentId());
            parentNode.getChildren().add(node);
        }

        return treeNodes.get(0);
    }
}
