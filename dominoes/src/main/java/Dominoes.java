import java.util.ArrayList;
import java.util.List;

class Dominoes {

    public List<Domino> formChain(List<Domino> dominoesList) throws ChainNotFoundException {
        ArrayList<Domino> chain = new ArrayList<>();
        if (!dominoesList.isEmpty()) {
            chain = formChainImpl(dominoesList.get(0).getLeft(), new ArrayList<>(dominoesList.subList(0, 1)),
                    new ArrayList<>(dominoesList.subList(1, dominoesList.size())), new ArrayList<>());
        }
        if (chain == null) {
            throw new ChainNotFoundException("No domino chain found.");
        }
        return chain;
    }

    ArrayList<Domino> formChainImpl(int start, ArrayList<Domino> created, ArrayList<Domino> remaining,
                                    ArrayList<Domino> removed) {
        if (remaining.isEmpty()) {
            return removed.isEmpty() && start == created.get(created.size() - 1).getRight()
                    ? created
                    : null;
        }

        Domino next = remaining.remove(0);
        ArrayList<Domino> found;
        if (next.getLeft() == created.get(created.size() - 1).getRight() || next.getRight() == created.get(created.size() - 1).getRight()) {
            Domino match = next.getRight() == created.get(created.size() - 1).getRight()
                    ? new Domino(next.getRight(), next.getLeft())
                    : next;

            created.add(match);
            remaining.addAll(removed);
            found = formChainImpl(start, created, remaining, new ArrayList<>());
            if (found != null) {
                return found;
            }
            remaining.removeAll(removed);
            created.remove(created.size() - 1);
        }

        removed.add(next);
        found = formChainImpl(start, created, remaining, removed);
        if (found != null) {
            return found;
        }
        removed.remove(removed.size() - 1);
        remaining.add(0, next);
        return null;
    }
}