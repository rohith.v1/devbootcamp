public class Twofer {
    public String twofer(String name) {
        final String template = "One for %s, one for me.";
        final String you = "you";

        Boolean isValid = name != null && !name.isEmpty() && !name.trim().isEmpty();
        return isValid
                ? String.format(template, name)
                : String.format(template, you);
    }
}
