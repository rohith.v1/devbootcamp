import java.util.List;

public class Knapsack {
    public int maximumValue(int limit, List<Item> items) {
        int[][] array = new int[items.size() + 1][limit + 1];

        for (int i = 0; i < items.size() + 1; i++) {
            for (int j = 0; j < limit + 1; j++) {
                array[i][j] = 0;

                if (i == 0 || j == 0) {
                    array[i][j] = 0;
                } else if (items.get(i - 1).getWeight() > j) {
                    array[i][j] = array[i - 1][j];
                } else {
                    int value = items.get(i - 1).getValue();
                    int weight = items.get(i - 1).getWeight();

                    array[i][j] = Math.max(array[i - 1][j], value + array[i - 1][j - weight]);
                }
            }
        }

        return array[items.size()][limit];
    }
}